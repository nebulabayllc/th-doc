1. software installation for Android test automation

Android Studio, and run android to download latest SDK
    http://developer.android.com/sdk/index.html

JDK with NetBeans
    http://www.oracle.com/technetwork/java/javase/downloads/jdk-netbeans-jsp-142931.html

VirtualBox
    https://www.virtualbox.org/wiki/Downloads

Vagrant
    https://www.vagrantup.com/downloads.html

wget
    https://eternallybored.org/misc/wget/

git
    https://git-scm.com/download/win

maven
    https://maven.apache.org/guides/getting-started/windows-prerequisites.html
    https://maven.apache.org/download.cgi


2. accounts

bitbucket
    https://bitbucket.org/

github
    https://github.com/


3. Virtual Machine setup
    https://github.com/tascape/testharness/tree/master/doc
    Please replace $HOME with your login home directory, such as c:\Users\yourname

    3.1
    create folder: $HOME\.th
    create folder: $HOME\qa\th

    3.2
    create file $HOME\.th\th.properties
    add content:
    # testharness system properties
    # use -Dkey=value to override or add in commandline
    qa.th.db.type=mysql
    qa.th.db.host=localhost:13306
    qa.th.log.path=$HOME\qa\th\logs
    qa.th.JOB_NAME=local-run
    qa.th.test.station=localhost

    3.3
    wget https://raw.githubusercontent.com/tascape/testharness/master/doc/Vagrantfile -O Vagrantfile
    vagrant up

    3.4
    open "http://localhost:18088/thr/suites_result.xhtml" in web browser
